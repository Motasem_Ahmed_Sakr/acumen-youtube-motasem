package acumen
package ui
package tl

import java.awt.{BorderLayout, Color}
import java.io.IOException
import java.util.concurrent.ConcurrentLinkedQueue
import javax.swing.event.{ChangeEvent, ChangeListener}
import javax.swing.{Box, JLabel, JOptionPane, JTextField, SwingUtilities}

import acumen.ui.interpreter.CStoreCntrl
import acumen.util.VideoRecord

import scala.reflect.io.File
import scala.swing._

class ControlButtons extends FlowPanel {

  case class VideoInfo(name:String, file:File)

  private val _recordThread = new Thread() {
    private val _waitTime = 1000
    private val _queueUpload = new ConcurrentLinkedQueue[VideoInfo]

    def putVideo(videoInfo: VideoInfo): Unit = {
      _queueUpload.add(videoInfo)
    }

    override def run() {
      while(this.isAlive) {
        if (_queueUpload.isEmpty) {
          Thread.sleep(_waitTime)
        } else {
          try {
            val videoInfo = _queueUpload.remove()
            val link = VideoRecord.uploadOnYoutube(Main.videoName, videoInfo.file,
              VideoRecord.youtubeInstance(Main.youtubeUser),
              Some(new VideoRecord.UploadLogger {
                override var progressListener: Double = _
              })
            ).get

            val runYoutubeUpload = new Runnable {
              private val _optionPanel = new JOptionPane(null, JOptionPane.PLAIN_MESSAGE, JOptionPane.CLOSED_OPTION) {
                val linkField = new JTextField()
                linkField.setEditable(false)
                linkField.setBackground(Color.WHITE)
                val content: Box = Box.createVerticalBox()
                content.add(new JLabel(" "))
                content.add(new JLabel("Video \"" + videoInfo.name + "\" was upload. Link to video on YouTube:"))
                content.add(new JLabel(" "))
                content.add(linkField)
              }

              private val _dialog = _optionPanel.createDialog("Success upload")
              _dialog.getContentPane.add(_optionPanel.content, BorderLayout.NORTH)
              _dialog.pack()

              override def run(): Unit = {
                _optionPanel.linkField.setText(link)
                _dialog.setVisible(true)
              }
            }

            SwingUtilities.invokeLater(runYoutubeUpload)
          } catch {
            case e: IOException => e.printStackTrace()
          }
        }
      }
    }
  }
  _recordThread.start()

  /* ---- definitions ------ */

  val play = new Action("play") {
    icon = Icons.play
    def apply(): Unit = App.ui.runSimulation()
    toolTip = "Run Simulation"
  }

  private var _recordNotEnd = true
  val record = new Action("record") {
    icon = Icons.record
    def apply(): Unit = {
      App.ui.threeDtab.checkRTAnimation.selected = true

      if (Main.videoDir.isEmpty) {
        App.ui.pickVideoDir
      }
      if (Main.videoDir.isDefined) {
        Main.videoRecordMode = Main.videoDir.isDefined
        App.ui.runSimulation()
        _recordNotEnd = false
      }
    }
    toolTip = "Record video"
  }
  val upload = new Action("Publish") {
    def apply(): Unit = {
      App.ui.threeDtab.checkRTAnimation.selected = true

      val recordFile = App.ui.interpreter.asInstanceOf[CStoreCntrl].lastRecordFile
      if (recordFile.isDefined) {
        _recordThread.putVideo(VideoInfo(Main.videoName, recordFile.get))
        enabled = false
      }
    }
    toolTip = "Available after Record"
  }
  val step = new Action("step") {
    icon = Icons.step
    def apply(): Unit = App.ui.stepSimulation()
    toolTip = "Compute one simulation step"
  }
  val pause = new Action("pause") {
    icon = Icons.pause
    def apply(): Unit = {
      _recordNotEnd = true
      App.ui.controller ! Pause
    }
    toolTip = "Pause simulation"
	
  }
  val stop = new Action("stop") {
    icon = Icons.stop
    def apply(): Unit = {
      _recordNotEnd = true
      Main.videoRecordMode = false
      App.ui.controller ! Stop
      App.ui.stopSimulation()
    }
    toolTip = "Stop simulation (cannot resume)"
  }

  val bPlay = new Button(play) { peer.setHideActionText(true) }
  val bRecord = new Button(record) { peer.setHideActionText(true) }
  val bUpload = new Button(upload) with ChangeListener {
    enabled = !_recordNotEnd
    peer.setHideActionText(false)

    override def stateChanged(e: ChangeEvent): Unit = enabled = false
  }
  val bStep = new Button(step) { peer.setHideActionText(true) }
  var bStop = new Button(stop) { peer.setHideActionText(true) }

  contents += new BoxPanel(Orientation.Vertical) {
    contents += new BoxPanel(Orientation.Horizontal) {
      contents ++= Seq(bPlay, bRecord, bStep, bStop)
    }
    peer.add(Box.createVerticalStrut(8))
    contents += new BorderPanel {
      add(bUpload, BorderPanel.Position.West)
    }
  }
  
  listenTo(App.pub)
  reactions += {
    case st:App.State => 
      play.enabled  = st match {case _:App.Playing => false; case _ => true}
      stop.enabled  = st match {case App.Stopped => false; case _ => true}
      pause.enabled = st match {case _:App.Playing => true; case _ => false}
      step.enabled  = st match {case _:App.Ready => true; case _ => false}
      record.enabled = st match {case _:App.Playing => false; case _ => true}
      bUpload.enabled = st match { case _:App.Playing => false; case _ => !_recordNotEnd }

      st match {
        case _:App.Ready =>
          bPlay.action = play
        case _:App.Playing =>
          bPlay.action = pause
      }
  }
}
